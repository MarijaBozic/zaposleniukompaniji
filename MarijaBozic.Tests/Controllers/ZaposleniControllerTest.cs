﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MarijaBozic.Interface;
using MarijaBozic.Models;
using MarijaBozic.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using System.Collections.Generic;
using System.Linq;

namespace MarijaBozic.Tests.Controllers
{
    [TestClass]
    public class ZaposleniControllerTest
    {

        [TestMethod]
        public void GetReturnsZaposleniWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<IZaposleniRepository>();
            mockRepository.Setup(x => x.GetById(15)).Returns(new Zaposleni { Id = 15, ImeIPrezime = "Zaposleni1", GodinaRodjenja = 1988, GodinaZaposlenja = 2008, Plata = 2000, KompanijaId = 1 });

            var controller = new ZaposleniController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetById(15);
            var contentResult = actionResult as OkNegotiatedContentResult<Zaposleni>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(15, contentResult.Content.Id);
        }

        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<IZaposleniRepository>();

            var controller = new ZaposleniController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Put(10, new Zaposleni { Id = 15, ImeIPrezime = "Zaposleni1", GodinaRodjenja = 1988, GodinaZaposlenja = 2008, Plata = 2000, KompanijaId = 1 });

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            // Arrange
            List<Zaposleni> listaZaposlenih = new List<Zaposleni>();
            listaZaposlenih.Add(new Zaposleni  { Id = 15, ImeIPrezime = "Zaposleni1", GodinaRodjenja = 1988, GodinaZaposlenja = 2008, Plata = 2000, KompanijaId = 1 });
            listaZaposlenih.Add(new Zaposleni { Id = 16, ImeIPrezime = "Zaposleni2", GodinaRodjenja = 1977, GodinaZaposlenja = 2006, Plata = 2500, KompanijaId = 2 });

            var mockRepository = new Mock<IZaposleniRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(listaZaposlenih.AsEnumerable());
            var controller = new ZaposleniController(mockRepository.Object);

            // Act
            IEnumerable<Zaposleni> result = controller.GetAll();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(listaZaposlenih.Count, result.ToList().Count);
            Assert.AreEqual(listaZaposlenih.ElementAt(1), result.ElementAt(1));
            Assert.AreEqual(listaZaposlenih.ElementAt(0), result.ElementAt(0));
        }

        [TestMethod]
        public void PostByPretraga()
        {
            //Arrange
            List<Zaposleni> listaZaposlenih = new List<Zaposleni>();
            listaZaposlenih.Add(new Zaposleni { Id = 15, ImeIPrezime = "Zaposleni1", GodinaRodjenja = 1988, GodinaZaposlenja = 2008, Plata = 2000, KompanijaId = 1 });
            listaZaposlenih.Add(new Zaposleni { Id = 16, ImeIPrezime = "Zaposleni2", GodinaRodjenja = 1977, GodinaZaposlenja = 2006, Plata = 2500, KompanijaId = 2 });

            Pretraga pretraga = new Pretraga() { Pocetak = 2001, Kraj = 2007 };

            var mockRepository = new Mock<IZaposleniRepository>();
            mockRepository.Setup(x => x.GetbyPretraga(2001, 2007)).Returns(listaZaposlenih.AsEnumerable());
            var controller = new ZaposleniController(mockRepository.Object);

            //Act
            dynamic response = controller.PostZaposlenje(pretraga);
            var result = (IEnumerable<dynamic>)response.Content;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(listaZaposlenih.Count, result.ToList().Count);
            Assert.AreEqual(listaZaposlenih.ElementAt(1), result.ElementAt(1));
            Assert.AreEqual(listaZaposlenih.ElementAt(0), result.ElementAt(0));
        }
    }

}
