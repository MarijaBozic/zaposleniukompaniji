﻿using MarijaBozic.Interface;
using MarijaBozic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MarijaBozic.Controllers
{
    public class ZaposleniController : ApiController
    {
        IZaposleniRepository _repository { get; set; }

        public ZaposleniController(IZaposleniRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Zaposleni> GetAll()
        {
            return _repository.GetAll();
        }

        public IHttpActionResult GetById(int id)
        {
            var zaposleni = _repository.GetById(id);
            if (zaposleni == null)
            {
                return NotFound();
            }

            return Ok(zaposleni);
        }

        public IHttpActionResult GetByGodiniRodjenja(int godiste)
        {
            var zaposleni = _repository.GetByGodinaRodjenja(godiste);
            if (zaposleni == null)
            {
                return NotFound();
            }

            return Ok(zaposleni);
        }

        [Route("api/statistika")]
        public IEnumerable<StatistikaDTO> GetByStatistika()
        {
            return _repository.GetByStatistika();
        }

        [Authorize]
        [Route("api/zaposlenje")]
        public IHttpActionResult PostZaposlenje(Pretraga pretraga)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (pretraga.Pocetak > pretraga.Kraj)
            {
                return BadRequest("Uneta vrednost u polje POCETAK mora biti manja od unete vrednosti u polje KRAJ");
            }

            var zaposleni = _repository.GetbyPretraga(pretraga.Pocetak, pretraga.Kraj);
            if (zaposleni == null)
            {
                return NotFound();
            }

            return Ok(zaposleni);
        }
        
        public IHttpActionResult Post(Zaposleni zaposleni)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _repository.Add(zaposleni);

            return CreatedAtRoute("DefaultApi", new { id = zaposleni.Id }, zaposleni);
        }
        [Authorize]
        public IHttpActionResult Put(int id, Zaposleni zaposleni)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != zaposleni.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(zaposleni);
            }
            catch
            {
                return BadRequest();
            }
            return Ok(zaposleni);
        }
        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            var zaposleni = _repository.GetById(id);
            if (zaposleni == null)
            {
                return NotFound();
            }
            _repository.Delete(zaposleni);
            return Ok();
        }
    }
}
