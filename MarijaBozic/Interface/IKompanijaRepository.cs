﻿using MarijaBozic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarijaBozic.Interface
{
    public interface IKompanijaRepository
    {
        IEnumerable<Kompanija> GetAll();
        Kompanija GetById(int id);
        IEnumerable<Kompanija> GetByTradicija();

    }
}
