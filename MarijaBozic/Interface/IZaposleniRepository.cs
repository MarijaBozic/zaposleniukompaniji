﻿using MarijaBozic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarijaBozic.Interface
{
    public interface IZaposleniRepository
    {
        IEnumerable<Zaposleni> GetAll();
        Zaposleni GetById(int id);
        IEnumerable<Zaposleni> GetByGodinaRodjenja(int godiste);

        IEnumerable<StatistikaDTO> GetByStatistika();
        IEnumerable<Zaposleni> GetbyPretraga(int pocetak, int kraj);

        void Add(Zaposleni zaposleni);
        void Update(Zaposleni zaposleni);
        void Delete(Zaposleni zaposleni);
    }
}
