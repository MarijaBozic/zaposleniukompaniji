﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MarijaBozic.Models
{
    public class StatistikaDTO
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public decimal ProsecnaPlata { get; set; }

    }
}