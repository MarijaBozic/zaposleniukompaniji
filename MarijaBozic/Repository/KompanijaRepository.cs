﻿using MarijaBozic.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MarijaBozic.Models;

namespace MarijaBozic.Repository
{
    public class KompanijaRepository:IDisposable, IKompanijaRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public IEnumerable<Kompanija> GetAll()
        {
            return db.Kompanije;
        }

        public IEnumerable<Kompanija> GetByTradicija()
        {
            List<Kompanija> listaKompanija = new List<Kompanija>();
             var komp1= db.Kompanije.OrderBy(x => x.GodinaOsnivanja).First();
            var komp2 = db.Kompanije.OrderByDescending(x => x.GodinaOsnivanja).First();
            listaKompanija.Add(komp1);
            listaKompanija.Add(komp2);

            return listaKompanija.OrderBy(x=>x.GodinaOsnivanja);

        }

        public Kompanija GetById(int id)
        {
            return db.Kompanije.FirstOrDefault(x => x.Id == id);
        }
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}