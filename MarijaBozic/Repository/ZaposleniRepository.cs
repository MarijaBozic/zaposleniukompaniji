﻿using MarijaBozic.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MarijaBozic.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace MarijaBozic.Repository
{
    public class ZaposleniRepository: IDisposable, IZaposleniRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        public IEnumerable<Zaposleni> GetAll()
        {
            return db.Zaposleni.Include(x => x.Kompanija).OrderByDescending(x => x.Plata);
        }

        public Zaposleni GetById(int id)
        {
            return db.Zaposleni.Include(x => x.Kompanija).FirstOrDefault(x => x.Id == id);
        }


        public IEnumerable<Zaposleni> GetByGodinaRodjenja(int godiste)
        {
            return db.Zaposleni.Include(x => x.Kompanija).Where(x => x.GodinaRodjenja > godiste).OrderBy(x => x.GodinaRodjenja);
        }

        public IEnumerable<StatistikaDTO> GetByStatistika()
        {

            IEnumerable<Zaposleni> zaposleni = GetAll();

            var rezultat = zaposleni.GroupBy(
                g => g.Kompanija,
                g => g.Plata,
                (kompanija, prosecnaPlata) => new StatistikaDTO()
                {
                    Id = kompanija.Id,
                    Naziv = kompanija.Naziv,
                    ProsecnaPlata = prosecnaPlata.Average()
                }).OrderByDescending(r => r.ProsecnaPlata);

            return rezultat.AsEnumerable();

        }
        

        public IEnumerable<Zaposleni> GetbyPretraga(int pocetak, int kraj)
        {
            return db.Zaposleni.Include(x => x.Kompanija).Where(x => x.GodinaZaposlenja >= pocetak && x.GodinaZaposlenja <=kraj).OrderBy(x => x.GodinaZaposlenja);
        }
        public void Add(Zaposleni zaposleni)
        {
            db.Zaposleni.Add(zaposleni);
            db.SaveChanges();
        }

        public void Delete(Zaposleni zaposleni)
        {
            db.Zaposleni.Remove(zaposleni);
            db.SaveChanges();
        }

        public void Update(Zaposleni zaposleni)
        {
            db.Entry(zaposleni).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}