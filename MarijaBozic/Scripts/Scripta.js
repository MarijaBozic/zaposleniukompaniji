﻿$(document).ready(function () {
    //-------------------------------------PODACI OD INTERESA -------------------------------------------------------------

    var host = window.location.host;
    var token = null;
    var headers = {};
    var zaposleniEndPoint = "/api/zaposleni/";
    var kompanijeEndPoint = "/api/kompanije/";
    var formAction = "Create";
    var editingId;

    //----------------------------------Pri pokretanju stranice odmah ucitavamo i podatke u tabelu------------------------------

    loadZaposleni();
    // -------------------PRIPREMAMO DOGADJAJE za BTN--------------------------------------------------------
    $("body").on("click", "#btnDelete", deleteZaposleni);
    $("body").on("click", "#btnEdit", editZaposleni);
    $("body").on("click", "#btnReg", prijavaRegistracija);
    $("body").on("click", "#btnPocetak", vracanjeNaPocetnu);
    $("body").on("click", "#btnPrijavaRegistracija", formaPrijaveRegistracije);
    $("body").on("click", "#odustajanje", odustajanjeEdit);
    $("body").on("click", "#odjavise", odjavljivanje);

    //----------------------------------------Ucitavanje liste Nekretnina  i agenata na stranicu ---------------------------------------------------=

    function loadZaposleni() {
        var requestUrl = 'http://' + host + zaposleniEndPoint;
        $.getJSON(requestUrl, setZaposleni);
    }
    //----------------------------Set dropDoown listu--------------------------------------------------------------
    function loadKompanije() {
        var requestUrl = 'http://' + host + kompanijeEndPoint;
        $.getJSON(requestUrl, setKompanije);
    }

    function setKompanije(data, status) {
        var $selectovanaOpcija = $("#kompanijaSelect");
        $selectovanaOpcija.empty();
        if (status === "success") {
            for (i = 0; i < data.length; i++) {
                var option = '<option value=' + data[i].Id + '>' + data[i].Naziv + '</option>';
                $selectovanaOpcija.append(option);
            }
        }
        else {
            alert("Greska prilikom ucitacanja kompanije!!!");
        }
    }
    //---------------------------------------KREIRANJE TABELE I UNOS PODATAKA U TABELU-----------------------------------------

    function setZaposleni(data, status) {
        var $container = $("#dataZaposleni");
        $container.empty();

        if (status === "success") {
            //---------------------------------------KREIRAMO ZAGLAVLJE TABELE ---------------------------------------------
            var div = $("<div></div>");
            var h1 = $("<h3 class='text-center'>Zaposleni</h3>");
            div.append(h1);
            var tabele = $("<table class='table table-bordered table-hover text-center'></table>");
            if (!token) {
                var header = $("<tr><th class='text-center bg-warning'>Ime i prezime</th><th class='text-center'>Godina rodjenja</th><th class='text-center'>Godina zaposlenja</th><th class='text-center'>Kompanija</th></tr>");
            }
            else {
                header = $("<tr><th class='text-center bg-warning'>Ime i prezime</th><th class='text-center'>Godina rodjenja</th><th class='text-center'>Godina zaposlenja</th><th class='text-center'>Kompanija</th>><th class='text-center'>Plata</th><th class='text-center'>Brisanje</th><th class='text-center'>Izmena</th></tr>");
            }
            tabele.append(header);

            //-------------------------PUNIMO TABELU PODACIMA------------------------------------------------------------------------------------
            for (i = 0; i < data.length; i++) {
                var row = "<tr>";
                if (!token) {
                    var displayData = "<td>" + data[i].ImeIPrezime + "</td><td>" + data[i].GodinaRodjenja + "</td><td>" + data[i].GodinaZaposlenja + "</td><td>" + data[i].Kompanija.Naziv + "</td>";
                }
                else {
                    var displayData1 = "<td>" + data[i].ImeIPrezime + "</td><td>" + data[i].GodinaRodjenja + "</td><td>" + data[i].GodinaZaposlenja + "</td><td>" + data[i].Kompanija.Naziv + "</td><td>" + data[i].Plata + "</td>";
                }

                var stringId = data[i].Id.toString();

                var displayDelete = "<td><button id=btnDelete name=" + stringId + ">Obrisi</button></td>";
                var displayEdit = "<td><button id=btnEdit name=" + stringId + ">Izmeni</button></td>";

                //-------------------------------PRIKAZ BUTTON-A ZA DELETE AKO JE KORISNIK USPESNO UOGOVAN---------------------------------------------------------
                if (!token) {
                    row += displayData + "</tr>";
                }
                else {

                    row += displayData1 + displayDelete + displayEdit + "</tr>";
                }
                tabele.append(row);
            }

            div.append(tabele);
            //-------------------------------PRIKAZ FORME ZA POST AKO JE KORISNIK USPESNO UOLGOVAN---------------------------------------------------------

            $container.append(div);
        }
        else {
            var div2 = $("<div></div>");
            var h12 = $("<h1>Greska prilikom prikazivanja podataka!!</h1>");
            div2.append(h12);
            $container.append(div2);
        }
    }

    // ---------------------REGISTRACIJA KORISNIKA------------------------------------------------------------------


    function prijavaRegistracija() {

        var email = $("#regEmail").val();
        var loz1 = $("#regLoz").val();
        var loz2 = $("#regLoz").val();


        var flagEemail = true;
        var flagLoz = true;
        $(".error").remove();

        if (!email) {
            $("#regEmail").after('<p><span class="error col-lg-offset-4" style="color:red">Ovo polje je obavezno!!!</span></p>');
            flagEemail = false;
        }
        if (!loz1) {
            $("#regLoz").after('<p><span class="error col-lg-offset-4" style="color:red">Ovo polje je obavezno!!!</span></p>');
            flagLoz = false;
        }

        if (flagEemail === false || flagLoz === false) {
            return;
        }

        // ---------------------------------OBJEKAT KOJI SE SALJE------------------------------------------------------
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz2
        };

        $.ajax({
            type: "POST",
            url: 'http://' + host + "/api/Account/Register",
            data: sendData

        }).done(function (data) {
            refreshPrijavaForm();
            alert("Uspesna registracija na sistem!");

        }).fail(function (data) {
            alert("Greska prilikom registracije!Proverite unos!");
        });

    }

    // ------------------PRIJAVA KORINSIKA------------------------------------------------------
    $("#prijava").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var loz = $("#regLoz").val();

        var flagEemail = true;
        var flagLoz = true;
        $(".error").remove();

        if (!email) {
            $("#regEmail").after('<p><span class="error col-lg-offset-4" style="color:red">Ovo polje je obavezno!!!</span></p>');
            flagEemail = false;
        }
        if (!loz) {
            $("#regLoz").after('<p><span class="error col-lg-offset-4" style="color:red">Ovo polje je obavezno!!!</span></p>');
            flagLoz = false;
        }

        if (flagEemail === false || flagLoz === false) {
            return;
        }

        // ------------------------------OBJEKAT KOJI SE SALJE-------------------------------------
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz
        };

        $.ajax({
            "type": "POST",
            "url": 'http://' + host + "/Token",
            "data": sendData

        }).done(function (data) {
            token = data.access_token;
           
            $("#regOrSignUp").addClass('hidden');
            $("#infoOdjava").removeClass('hidden');
            $("#info").empty().append("Prijavljen korisnik: " + data.userName);
            $("#pretraga").removeClass('hidden');

            refreshPrijavaForm();
            loadZaposleni();

        }).fail(function (data) {
            alert("Greska prilikom prijave!!");
        });
    });

    //----------------------------------------------PRETRAGA------------------------------------------------------------------------------

    $("#formPretraga").submit(function (e) {
        e.preventDefault();

        if (token) {
            var data = getPretraga();
            $('#startPretraga').val();
            $('#krajPretraga').val();
        }
        else {
            alert("Morate biti ulogovani.");
        }
    });


    function getPretraga() {
        if (token) {
            headers.Authorization = 'Bearer ' + token;

            var pocetak = $('#startPretraga').val();
            var kraj = $('#krajPretraga').val();

            var flagMini = true;
            var flagMaksi = true;

            $(".error").remove();

            if (pocetak <= 1 || !pocetak) {
                $("#startPretraga").after('<p><span class="error col-sm-offset-4" style="color:red">Ovo polje je obavezno!!!</span></p>');
                flagMini = false;
            }
            if (!kraj) {
                $("#krajPretraga").after('<p><span class="error col-sm-offset-4" style="color:red">Ovo polje je obavezno!!!</span></p>');
                flagMaksi = false;
            }

            if (flagMini === false || flagMaksi === false) {
                return;
            }

            // ------------------------------------------------------------objekat koji se salje----------------------------------------------------------
            let sendData = {
                "Pocetak": pocetak,
                "Kraj": kraj
            };

            $.ajax({
                type: "POST",
                url: 'http://' + host + "/api/zaposlenje",
                data: sendData,
                "headers": headers
            })
                .done(function (data, status) {
                    setZaposleni(data, status);//-------------------------------------PUNI TABELU SA PODACIMA IZ PRETRAGE---------------------------------------------

                  
                })
                .fail(function (data) {
                    alert("Greska prilikom pretrage!");
                });
        }
        else {
            alert("Morate biti ulogovani.");
        }
    }





    // ----------------------------EDIT ZAPOSLENIH--------------------------------------------------------------------------

    $("#editZaposlenog").submit(function (e) {
        e.preventDefault();


        var kompanija = $("#kompanijaSelect").val();
        var imeIPrezime = $("#imeIprezime").val();
        var godinaRodjenja = $("#godRodjenja").val();
        var godinaZaposlenja = $("#godZaposlenja").val();
        var plata = $("#plata").val();


        var httpAction;
        var sendData;
        var url;
        // -------------------------------MORAMO BITI ULOGOVANI-------------------------------------------------------------------------
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        //--------------------------------KLIJENTSKA VALIDACIJA-------------------------------------------------------------------------
        var flagKompanija = true;
        var flagImeIPrezime = true;
        var flagGodinaRodjenja = true;
        var flagGodinaZaposlenja = true;
        var flagPlata = true;
       

        $(".error").remove();

        if (!kompanija) {
            $("#kompanijaSelect").after('<p><span class="error col-lg-offset-4" style="color:red">Ovo polje je obavezno!!</span></p>');
            flagKompanija = false;
        }

        if (imeIPrezime.length < 1 || imeIPrezime.length > 50) {
            $("#imeIprezime").after('<p><span class="error col-lg-offset-4" style="color:red">Uneta vrednost mora biti u intervalu od 1 do 60!!</span></p>');
            flagImeIPrezime = false;
        }


        if (godinaRodjenja < 1952 || godinaRodjenja > 1990) {
            $("#godRodjenja").after('<p><span class="error col-lg-offset-4" style="color:red">Uneta vrednost mora biti u intervalu od 1952 do 1990!!!</span></p>');
            flagGodinaRodjenja = false;
        }

        if (godinaZaposlenja <=2000) {
            $("#godZaposlenja").after('<p><span class="error col-lg-offset-4" style="color:red">Uneta vrednost mora biti veca od 2000!!</span></p>');
            flagGodinaZaposlenja = false;
        }

        if (plata <= 2000 || plata >=10000) {
            $("#plata").after('<p><span class="error col-lg-offset-4" style="color:red">Uneta vrednost mora biti veca od 2000 a manja od 10000!!</span></p>');
            flagPlata = false;
        }



        if (flagKompanija === false || flagImeIPrezime === false || flagGodinaRodjenja === false || flagGodinaZaposlenja === false || flagPlata === false) {
            return;
        }

        //----------------------------------PRIPREMAMO OBJEKAT----------------------------------------------------------------

        if (formAction === "Create") {
            httpAction = "POST";
            url = 'http://' + host + zaposleniEndPoint;

            sendData = {
                "KompanijaId": kompanija,
                "ImeIPrezime": imeIPrezime,
                "GodinaRodjenja": godinaRodjenja,
                "GodinaZaposlenja": godinaZaposlenja,
                "Plata": plata
            };
        }
        else {
            httpAction = "PUT";
            url = 'http://' + host + zaposleniEndPoint + editingId.toString();
            sendData = {
                "Id": editingId,
                "KompanijaId": kompanija,
                "ImeIPrezime": imeIPrezime,
                "GodinaRodjenja": godinaRodjenja,
                "GodinaZaposlenja": godinaZaposlenja,
                "Plata": plata
            };
        }

        $.ajax({
            url: url,
            type: httpAction,
            headers: headers,
            data: sendData
        })
            .done(function (data, status) {
                formAction = "Create";
                $(".error").remove();
                loadZaposleni();
                $("#editZaposlenog").addClass('hidden');

            })
            .fail(function (data, status) {
                alert("Greska prilikom izmene!!!");
            });
    });


    //----------------------------------------------BRISANJE--------------------------------------------------------------
    function deleteZaposleni() {
        var deleteId = this.name;

        // --------------------------------------MORAMO BITI ULOGOVANI-------------------------------------------------------
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }
        //----------------------------------------------SALJEMO ZAHTEV-----------------------------------------------------
        $.ajax({
            url: 'http://' + host + zaposleniEndPoint + deleteId.toString(),
            type: "DELETE",
            headers: headers
        })
            .done(function (data, status) {
                loadZaposleni();
            })
            .fail(function (data, status) {
                alert("Greska prilikom brisanja!!!");
            });
    }
    //----------------------------------------------EDIT------------------------------------------------------------------------------
    function editZaposleni() {
        var editId = this.name;
        $("#editZaposlenog").removeClass('hidden');
        loadKompanije();
        // --------------------------------------MORAMO BITI ULOGOVANI-------------------------------------------------------
        if (token) {
            headers.Authorization = 'Bearer ' + token;
        }

        //----------------------------------------------SALJEMO ZAHTEV-----------------------------------------------------
        $.ajax({
            url: 'http://' + host + zaposleniEndPoint + editId.toString(),
            type: "GET",
            headers: headers

        })
            .done(function (data, status) {
                $("#kompanijaSelect").val(data.KompanijaId);
                $("#imeIprezime").val(data.ImeIPrezime);
                $("#godRodjenja").val(data.GodinaRodjenja);
                $("#godZaposlenja").val(data.GodinaZaposlenja);
                $("#plata").val(data.Plata);

                editingId = data.Id;
                formAction = "Update";
            })
            .fail(function (data, status) {
                formAction = "Create";
                alert("Greska prilikom izmene!!!");
            });
    }

    function odustajanjeEdit() {
        $("#editZaposlenog").addClass('hidden');

    }

    function formaPrijaveRegistracije() {
        $("#infoPocetnaStrana").addClass('hidden');
        $("#infoRegIPrijava").removeClass('hidden');
        $("#regOrSignUp").removeClass('hidden');
    }

    function vracanjeNaPocetnu() {

        $("#infoPocetnaStrana").removeClass('hidden');
        $("#infoRegIPrijava").addClass('hidden');
        $("#regOrSignUp").addClass('hidden');
        $(".error").remove();
        refreshPrijavaForm();
    }
    function refreshPrijavaForm() {
        $("#regEmail").val('');
        $("#regLoz").val('');
    }

    function odjavljivanje() {

        token = null;
        headers = {};

        $("#infoPocetnaStrana").removeClass('hidden');
        $("#infoOdjava").addClass('hidden');
        $("#pretraga").addClass('hidden')
        $("#editZaposlenog").addClass('hidden');
        loadZaposleni();
    }

});